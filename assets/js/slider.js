$(".itens").carouFredSel({
    circular: true,
    infinite: true,
    auto    : false,
    responsive: true,
    height:"auto !important",
    position : "relative",
    items:4,
    prev    : {
        button  : "#prev",
        key     : "left"
    },
    next    : {
        button  : "#next",
        key     : "right"
    }
});
