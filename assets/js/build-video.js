/*
   Função addLink > Adiciona uma
   lista de links dinâmicamente
   a partir do json description.js
*/

function addLink() {
    var slider, ul, qVideo, qID, qAtual, i, li, a, img, id;

    slider = document.querySelector(".slider");
    ul = document.createElement("ul");
    qVideo = window.location.href;
    qID = qVideo.replace(/(.+)?\?(.+)?$/g, "$2");
    qAtual = BWS[qID];
    i = 0;

    slider.appendChild(ul);

    for (i = 0; i < qAtual.length; i += 1) {
      //criando elemento
        li = document.createElement("li");
        a = document.createElement("a");
        img = document.createElement("img");
        id = qAtual[i].id;
      //setando atributos
        ul.setAttribute("class", "itens");
        li.setAttribute("id", i + 1);
        a.setAttribute("href", "");
      // a.setAttribute("target","_blank");
        img.setAttribute("src", "https://i1.ytimg.com/vi/" + id + "/mqdefault.jpg");

      //adicionando no html
        ul.appendChild(li);
        li.appendChild(a);
        a.appendChild(img);
    }
}

/*
   Função acharLinks > Localiza no Dom
   os nossos links criados dinamicamente
   e quando clicarmos em cada um deles
   puxamos a parte adequada do JSON
*/


function makeClick(i) {
	return function (e) {
		var el = this.parentNode.id;
		feedContent(el);
		e.preventDefault();
	};
}

function acharLinks() {
	var thumb, links, i;
    thumb = document.querySelector('.itens');
    links = thumb.getElementsByTagName('a');
    i = 0;


    for (i = 0; i< links.length; i += 1) {
	  links[i].addEventListener("click", makeClick(i));
    }

}

/*
	Função feedContent > Função que alimenta nosso
	HTML criado pela função addLinks
*/
function feedContent(el) {
	var qVideo = window.location.href;
	var qID = qVideo.replace(/(.+)?\?(.+)?$/g, "$2");
	var qAtual = BWS[qID];
	var quem = parseInt(el);
	var qSrc = "http://www.youtube.com/embed/";

	var quem = quem - 1;

	var _title = qAtual[quem].title;

	var _destino = qSrc + qAtual[quem].id + "?enablejsapi=1&amp;" + "autoplay=" + config.autoplay + "&amp;wmode=transparent&" + config.hd + config.cc;

	var _desc = qAtual[quem].description;


	var titulo = document.getElementById("title");
	titulo.innerHTML = _title;

	var embed = document.getElementById("video-holder");
	embed.src = _destino;

	var descricao = document.getElementById("desc");
	descricao.innerHTML = _desc;

};

addLink();
acharLinks();

